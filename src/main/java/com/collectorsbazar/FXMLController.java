package com.collectorsbazar;

import java.net.URL;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXAlert;
import com.jfoenix.controls.JFXDatePicker;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.stage.Modality;

public class FXMLController implements Initializable {
    
    @FXML
    private Label label;

    @FXML
    private JFXDatePicker dp;
    
    @FXML
    private void handleButtonAction(ActionEvent event) {
        System.out.println("You clicked me!");
        label.setText("Hello World!");
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        dp = new JFXDatePicker();
        dp.setVisible(true);
//        dp.setOnAction(e ->{
//            System.out.println(dp.getValue());
//        });
    }
}
